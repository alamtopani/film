module SeedUser
  def self.seed
    User.find_or_create_by!(email: "admin@film.com") do |admin|
      admin.username = "adminmaster"
      admin.password = "12345678"
      admin.password_confirmation = "12345678"
      admin.type = "Admin"
      admin.confirmation_token = nil
      admin.confirmed_at= Time.now
    end

    Member.find_or_create_by!(email: "member@film.com") do |admin|
      admin.username = "member1"
      admin.password = "12345678"
      admin.password_confirmation = "12345678"
      admin.type = "Member"
      admin.confirmation_token = nil
      admin.confirmed_at= Time.now
    end
  end
end