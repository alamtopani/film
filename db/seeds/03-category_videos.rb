module SeedCategoryVideo
  CATEGORIES =  ['HOLLYWOOD MOVIE', 'FILM KOREA', 'FILM JEPAN', 'FILM THAILAND', 'FILM INDONESIA', 'ANIME MOVIE', 'BOLLYWOOD MOVIE']
  def self.seed
    CATEGORIES.each do |c|
      CategoryVideo.find_or_create_by!(name: c) do |category_video|
        category_video.cover_image = File.new("#{Rails.root}/app/assets/images/cover.jpg")
        category_video.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean venenatis hendrerit fringilla. Sed ornare luctus nisi, nec fermentum purus vehicula ut."
      end
    end
  end
end