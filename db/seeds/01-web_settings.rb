module SeedWebSetting
  def self.seed
    WebSetting.find_or_create_by({
      header_tags: "Zona Film",
      footer_tags: "© 2016 ZONA FILM. All Rights Reserved.",
      contact: '+6221-8800-9898',
      email: 'hi@film.com',
      facebook: 'http://www.facebook.com/',
      twitter: 'http://www.twitter.com/',
      title: "Zona Film",
      keywords: "Zona Film",
      description: '',
      author: "@zonafilm",
    })
  end
end
