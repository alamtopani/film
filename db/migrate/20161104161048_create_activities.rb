class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :title
      t.string :activitiable_type
      t.integer :activitiable_id
      t.integer :user_id
      t.string :link

      t.timestamps null: false
    end

    add_index :activities, :user_id
    add_index :activities, :activitiable_id
  end
end
