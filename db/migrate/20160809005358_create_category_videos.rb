class CreateCategoryVideos < ActiveRecord::Migration
  def change
    create_table :category_videos do |t|
      t.string :name
      t.string :slug
      t.string :cover_image
      t.text :description

      t.timestamps null: false
    end
  end
end
