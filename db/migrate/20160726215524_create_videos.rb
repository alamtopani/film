class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.string :slug
      t.string :director
      t.string :actors
      t.integer :category_video_id
      t.text :description
      t.string :cover_image
      t.string :file
      t.integer :session
      t.integer :user_id
      t.string :code
      t.string :tag
      t.string :year
      t.string :type_video
      t.boolean :status, default: false
      t.boolean :featured, default: false
      t.string :kind, default: 'Movies'
      t.integer :view_count, default: 0

      t.timestamps null: false
    end

    add_index :videos, :user_id
    add_index :videos, :category_video_id
  end
end
