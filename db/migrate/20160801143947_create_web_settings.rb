class CreateWebSettings < ActiveRecord::Migration
  def change
    create_table :web_settings do |t|
      t.string :title
      t.text :description
      t.text :keywords
      t.string :header_tags
      t.string :footer_tags
      t.string :contact
      t.string :email
      t.string :favicon
      t.string :logo
      t.string :facebook
      t.string :twitter
      t.string :author
      t.string :longitude
      t.string :latitude
      t.text :address

      t.timestamps null: false
    end
  end
end
