class AddDownloadInVideo < ActiveRecord::Migration
  def change
    add_column :videos, :link_download_video, :string
    add_column :videos, :click_link_download_video, :integer
    add_column :videos, :link_download_subtitle, :string
    add_column :videos, :click_link_download_subtitle, :integer

    add_column :episodes, :link_download_video, :string
    add_column :episodes, :click_link_download_video, :integer
    add_column :episodes, :link_download_subtitle, :string
    add_column :episodes, :click_link_download_subtitle, :integer
  end
end
