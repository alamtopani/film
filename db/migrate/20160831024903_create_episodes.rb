class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :title
      t.integer :episode
      t.string :link_video
      t.string :episodeable_type
      t.integer :episodeable_id
      t.string :cover_image

      t.timestamps null: false
    end
  end
end
