class AddCredentialTokenInUser < ActiveRecord::Migration
  def change
    add_column :users, :twitter_uid, :string
    add_column :users, :twitter_token, :string
    add_column :users, :twitter_secret, :string

    add_column :users, :facebook_uid, :string
    add_column :users, :facebook_oauth_token, :string
    add_column :users, :facebook_oauth_token_expires, :string

    add_column :users, :instagram_uid, :string
    add_column :users, :instagram_oauth_token, :string
    add_column :users, :instagram_oauth_token_expires, :string

    add_column :users, :google_uid, :string
    add_column :users, :google_oauth_token, :string
    add_column :users, :google_oauth_token_expires, :string

    add_column :users, :facebook_friends_count, :integer
    add_column :users, :instagram_followers_count, :integer
    add_column :users, :twitter_followers_count, :integer
  end
end
