class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :born
      t.string :gender
      t.string :country
      t.string :province
      t.string :city
      t.string :phone
      t.text :address
      t.string :from
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :profiles, :user_id
  end
end
