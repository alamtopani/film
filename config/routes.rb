Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :users,
    controllers: {
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions'
    },
    path_names: {
      sign_in:  'login',
      sign_out: 'logout',
      sign_up:  'register'
    }
    
  root 'public#home'
  get "sitemap.xml" => "sitemaps#index", format: "xml", as: 'sitemap'
  
  resources :videos do
    collection do
      get :episode
      get :results
    end
  end
  resources :wishlists

  namespace :userpage do
    get 'dashboard', to: 'home#dashboard', as: 'dashboard'
    resources :members
    resources :videos
    resources :activities
    resources :wishlists
  end

  namespace :backend do
    get "dashboard", to: 'home#dashboard', as: 'dashboard_admin'
    resources :admins
    resources :members do
      collection do
        get :multiple_action
      end
    end
    resources :category_videos
    resources :videos do
      collection do
        get :multiple_action
      end
    end
    resources :web_settings
  end
end
