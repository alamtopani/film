Devise.setup do |config|
  config.mailer_sender = 'movies.series.com@gmail.com'

  require 'devise/orm/active_record'
  require "omniauth-facebook"
  require 'omniauth-google-oauth2'
  require 'omniauth-twitter'

  config.omniauth :facebook, '1778255869109142', '01e2be5f329c4a96942602fe300fffd9', {:scope => 'public_profile publish_actions'}
  config.omniauth :google_oauth2, "288327438888-tdddip5a5217pllohut5gis1ovmj6tfq.apps.googleusercontent.com", "yWn4yIgyyMA4iKBz6IXI85jC", :scope => "userinfo.email,userinfo.profile", provider_ignores_state: true 

  config.authentication_keys = [:username]
  config.case_insensitive_keys = [:username]
  config.strip_whitespace_keys = [:username]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 6..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
end
