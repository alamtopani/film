FactoryGirl.define do
  factory :profile do
    first_name "MyString"
    last_name "MyString"
    born "2016-07-25"
    gender "MyString"
    country "MyString"
    province "MyString"
    city "MyString"
    phone "MyString"
    address "MyText"
    from "MyString"
    user_id 1
  end
end
