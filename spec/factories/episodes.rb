FactoryGirl.define do
  factory :episode do
    episode 1
    link_video "MyString"
    episodeable_type "MyString"
    episodeable_id 1
  end
end
