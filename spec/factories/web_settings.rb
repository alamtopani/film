FactoryGirl.define do
  factory :web_setting do
    title "MyString"
    description "MyText"
    keywords "MyString"
  end
end
