FactoryGirl.define do
  factory :video do
    title "MyString"
    director "MyString"
    actors "MyString"
    category "MyString"
    description "MyText"
    cover_image "MyString"
    file "MyString"
    session "MyString"
    episode "MyString"
    user_id ""
    code "MyString"
    tag "MyString"
  end
end
