FactoryGirl.define do
  factory :category_video do
    name "MyString"
    slug "MyString"
    cover_image "MyString"
    description "MyText"
  end
end
