FactoryGirl.define do
  factory :activity do
    title "MyString"
    activitiable_type "MyString"
    activitiable_id 1
    user_id 1
    link "MyString"
  end
end
