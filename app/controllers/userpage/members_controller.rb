class Userpage::MembersController < Userpage::ApplicationController
  before_filter :draw_password, only: :update

  def edit
    resource
  end

  def update
    resource
    if @member.update(permitted_params)
      redirect_to :back, notice: 'Your profile have been updated!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  private
    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:member].delete(attr)
      end if params[:member] && params[:member][:password].blank?
    end

    def resource
      @member = User.find current_user
    end

    def permitted_params
      params.require(:member).permit(
                                    :username, 
                                    :email, 
                                    :password, 
                                    :password_confirmation,
                                    :type,
                                    :avatar,
                                    :code,
                                    profile_attributes: [
                                      :first_name,
                                      :last_name,
                                      :born,
                                      :gender,
                                      :country,
                                      :province,
                                      :city,
                                      :phone,
                                      :address,
                                      :from,
                                      :user_id
                                    ])
    end
end