class Userpage::HomeController < Userpage::ApplicationController
  def dashboard
    @activities = current_user.activities.latest.paginate(page: page, per_page: per_page)
    if current_user.verified?
      @videos = current_user.videos.latest.limit(15)
    else
      @videos = current_user.wishlists.latest.limit(15)
    end
  end
end