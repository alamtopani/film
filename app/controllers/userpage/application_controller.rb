class Userpage::ApplicationController < ApplicationController
  before_filter :authenticate_user!
  before_action :prepare_for_videos

  layout 'userpage'

  def authenticate_user!
    unless current_user.present? && current_user.is_member?
      redirect_to root_path, alert: "Can't Access this page"
    else
      @count_videos = current_user.videos
    end
  end

  def prepare_for_videos
    @popular_videos = Video.actived.popular.limit(8)
  end
end