class Userpage::WishlistsController < Userpage::ApplicationController

  def index
    @wishlists = collection.paginate(page: page, per_page: 1)
    @videos_count = collection.size
  end

  def destroy
    resource
    if @wishlist.destroy
      redirect_to :back, notice: 'Your wishlist video have been deleted!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  private
    def resource
      @wishlist = current_user.wishlists.find params[:id]
    end

    def collection
      @collection = current_user.wishlists.latest.search_by(params)
    end
end