class Userpage::VideosController < Userpage::ApplicationController

  def index
    @videos = collection.paginate(page: page, per_page: per_page)
    @videos_count = collection.size
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new(permitted_params)
    @video.user_id = current_user.id
    if @video.save
      redirect_to userpage_videos_path, notice: 'Your video have been created!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def edit
    resource
  end

  def update
    resource
    if @video.update(permitted_params)
      redirect_to userpage_videos_path, notice: 'Your video have been updated!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def destroy
    resource
    if @video.destroy
      redirect_to :back, notice: 'Your video have been deleted!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  private
    def resource
      @video = current_user.videos.find params[:id]
    end

    def collection
      @collection = current_user.videos.latest.search_by(params)
    end

    def permitted_params
      params.require(:video).permit(
                                    :id,
                                    :title, 
                                    :director, 
                                    :actors, 
                                    :category,
                                    :description,
                                    :cover_image,
                                    :file,
                                    :session,
                                    :user_id,
                                    :code,
                                    :tag,
                                    :year,
                                    :status,
                                    :featured,
                                    :type_video,
                                    :kind,
                                    :category_video_id,
                                    :link_download_video,
                                    :click_link_download_video,
                                    :link_download_subtitle,
                                    :click_link_download_subtitle,
                                    episodes_attributes: [
                                      :id,
                                      :title,
                                      :episode,
                                      :link_video,
                                      :cover_image,
                                      :link_download_video,
                                      :click_link_download_video,
                                      :link_download_subtitle,
                                      :click_link_download_subtitle,
                                      :_destroy
                                    ])
    end
end