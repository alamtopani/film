class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    @user = User.find_for_devise_oauth(request.env["omniauth.auth"], current_user)
    
    if @user == 'Error'
      redirect_to new_user_session_path, alert: 'Username already being used by Another Account!'
    elsif @user == 'Error Current'
      redirect_to root_path, alert: 'Your Facebook account is already used another user, please use a different facebook accounts!'
    else
      if @user.present?
        unless current_user.present?
          sign_in_and_redirect @user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
        else
          sign_in_and_redirect current_user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
        end
      else
        session["devise.facebook_data"] = request.env["omniauth.auth"]
        redirect_to new_user_session_path
      end
    end
  end

  def google_oauth2
    @user = User.find_for_devise_oauth(request.env["omniauth.auth"], current_user)

    if @user == 'Error'
      redirect_to new_user_session_path, alert: 'Username already being used by Another Account!'
    elsif @user == 'Error Current'
      redirect_to root_path, alert: 'Your Gmail account is already used another user, please use a different gmail accounts!'
    else
      if @user.present?
        unless current_user.present?
          sign_in_and_redirect @user, :event => :authentication
          flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
        else
          sign_in_and_redirect current_user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "Google") if is_navigational_format?
        end
      else
        session["devise.google_data"] = request.env["omniauth.auth"]
        redirect_to new_user_session_path
      end
    end
  end

  def twitter
    @user = User.find_for_devise_oauth(request.env["omniauth.auth"], current_user)

    if @user == 'Error'
      redirect_to new_user_session_path, alert: 'Username Sudah di Gunakan Oleh Akun Lain!'
    elsif @user == 'Error Current'
      redirect_to root_path, alert: 'Akun twitter anda sudah digunakan user lain, silahkan gunakan akun yang twitter berbeda!'
    else
      if @user.present?
        unless current_user.present?
          sign_in_and_redirect @user, :event => :authentication
          flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Twitter"
        else
          sign_in_and_redirect current_user, :event => :authentication
          set_flash_message(:notice, :success, :kind => "Twitter") if is_navigational_format?
        end
      else
        session["devise.twitter_data"] = request.env["omniauth.auth"]
        redirect_to new_user_session_path
      end
    end
  end
  
end