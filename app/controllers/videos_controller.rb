class VideosController < ApplicationController  
  def index
    @category_video = CategoryVideo.find(params[:category])
    @collection = @category_video.videos.actived.top_position.by_year.latest.search_by(params)
    @videos = @collection.paginate(page: page, per_page: per_page)
    
    @popular_videos = @category_video.videos.actived.popular.limit(8)
    @featured_videos = @category_video.videos.actived.featured.updatest.limit(6)
    impressionist(@category_video, 'message')
  end

  def show
    @video = Video.find_by(slug: params[:id])

    if @video.present?
      prepare_quick_count(@video)
      @popular_videos = Video.actived.popular.limit(8)
      @other_videos = collection.where(category_video_id: @video.category_video_id).limit(10)
      impressionist(@video, 'message')

      if current_user.present?
        prepare_activity("Has been viewed video <a href='#{video_url(@video)}'>#{@video.title}</a>", 'Video', @video.id, video_url(@video), current_user)
      end
    else
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  def episode
    @video = Video.find_by(slug: params[:video])
  
    if @video.present?
      @episode = @video.episodes.find_by(episode: params[:episode])

      if @episode.present?
        @popular_videos = Video.actived.popular.limit(8)
        impressionist(@episode, 'message')

        if current_user.present?
          prepare_activity("Has been viewed series video <a href='#{episode_videos_path(episode: @episode.episode, video: @video.slug)}'>#{@episode.title}</a>", 'Episode', @episode.id, episode_videos_path(episode: @episode.episode, video: @video.slug), current_user)
        end
      else
        redirect_to root_path, alert: "Can't Access this page"
      end
    else
      redirect_to root_path, alert: "Can't Access this page"
    end
  end

  def results
    @collection = collection
    @videos = @collection.paginate(page: page, per_page: per_page)
    @popular_videos = Video.actived.popular.limit(8)
  end

  private
    def resource
      @video = Video.find params[:id]
    end

    def collection
      @collection = Video.actived.by_year.latest.search_by(params)
    end

    def prepare_quick_count(resource)
      resource = Video.find(resource)
      resource.view_count = resource.view_count+1
      resource.save
    end
end