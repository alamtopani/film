class Backend::CategoryVideosController < Backend::ApplicationController
  before_action :class_name
  add_breadcrumb "Category Videos", :backend_category_videos_path

  def index
    @category_videos = collection.paginate(page: page, per_page: per_page)
  end

  def new
    prepare_add_breadcrumb_action
    @category_video = CategoryVideo.new
  end

  def create
    @category_video = CategoryVideo.new(permitted_params)
    if @category_video.save
      redirect_to :back, notice: 'Your category video have been created!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def edit
    prepare_add_breadcrumb_action
    resource
  end

  def update
    resource
    if @category_video.update(permitted_params)
      redirect_to :back, notice: 'Your category video have been updated!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def destroy
    resource
    if @category_video.destroy
      redirect_to :back, notice: 'Your category video have been deleted!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  private
    def resource
      @category_video = CategoryVideo.find params[:id]
    end

    def collection
      @collection = CategoryVideo.latest
    end

    def permitted_params
      params.require(:category_video).permit(
                                    :id,
                                    :name, 
                                    :slug, 
                                    :cover_image, 
                                    :description
                                    )
    end

    def class_name
      @resource_name = 'Category Video'
      @collection_name = 'Category Videos'
    end
end