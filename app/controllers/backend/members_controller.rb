class Backend::MembersController < Backend::ApplicationController 
  before_action :class_name
  before_filter :draw_password, only: :update

  add_breadcrumb "Members", :backend_members_path

  def index
    add_breadcrumb "#{params[:action]}"
    @members = collection.paginate(page: page, per_page: per_page)
    @members_count = collection.size
  end

  def show
    prepare_add_breadcrumb_action
    resource
  end

  def new
    prepare_add_breadcrumb_action
    add_breadcrumb "#{params[:action]}"
    @member = Member.new
  end

  def create
    @member = Member.new(params_permit)

    if @member.save
      redirect_to :back, notice: "Successfully saved #{@resource_name}"
    else
      redirect_to :back
    end
  end

  def edit
    prepare_add_breadcrumb_action
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    if @member.update(params_permit)
      redirect_to :back, notice: "Successfully saved #{@resource_name}"
    else
      redirect_to :back
    end
  end

  def destroy
    resource

    if @member.destroy
      redirect_to :back
    else
      redirect_to :back
    end
  end

  def multiple_action
    if params[:ids].present?
      if params[:commit] == 'Do Verified'
        
        collect = params[:ids]

        Member.where(id: collect).each do |c|
          c.verified = true
          c.save
        end

        redirect_to :back, alert: 'Successfully set verified Members!'
      elsif params[:commit] == 'Do Featured'
        
        collect = params[:ids]

        Member.where(id: collect).each do |c|
          c.featured = true
          c.save
        end

        redirect_to :back, alert: 'Successfully set featured Members!'
      elsif params[:commit] == 'Do Destroy'
        collect = params[:ids]

        Member.where(id: collect).each do |c|
          c.destroy
        end

        redirect_to :back, alert: 'Successfully deleted Members!'
      else
        redirect_to :back, alert: 'Nothing checked!'
      end 
    end
  end

  private
    def resource
      @member = Member.find(params[:id])
    end

    def collection
      @member = Member.latest.search_by(params)
    end

    def params_permit
      params.require(:member).permit(
                                    :username, 
                                    :email, 
                                    :password, 
                                    :password_confirmation,
                                    :type,
                                    :avatar,
                                    :code,
                                    :verified,
                                    :featured,
                                    profile_attributes: [
                                      :first_name,
                                      :last_name,
                                      :born,
                                      :gender,
                                      :country,
                                      :province,
                                      :city,
                                      :phone,
                                      :address,
                                      :from,
                                      :user_id
                                    ])
    end

    def class_name
      @resource_name = 'Member'
      @collection_name = 'Members'
    end

    def draw_password
      %w(password password_confirmation).each do |attr|
        params[:member].delete(attr)
      end if params[:member] && params[:member][:password].blank?
    end
end
