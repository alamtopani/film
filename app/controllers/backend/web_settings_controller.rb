class Backend::WebSettingsController < Backend::ApplicationController 
  before_action :class_name
  add_breadcrumb "Web Settings", :backend_web_settings_path

  def index
    @web_settings = collection
  end

  def show
    prepare_add_breadcrumb_action
    resource
  end

  def new
    prepare_add_breadcrumb_action
    add_breadcrumb "#{params[:action]}"
    @web_setting = WebSetting.new
  end

  def create
    @web_setting = WebSetting.new(params_permit)

    if @web_setting.save
      redirect_to :back, notice: "Successfully saved #{@resource_name}"
    else
      redirect_to :back
    end
  end

  def edit
    prepare_add_breadcrumb_action
    add_breadcrumb "#{params[:action]}"
    resource
  end

  def update
    resource

    if @web_setting.update(params_permit)
      redirect_to :back, notice: "Successfully saved #{@resource_name}"
    else
      redirect_to :back
    end
  end

  def destroy
    resource

    if @web_setting.destroy
      redirect_to :back
    else
      redirect_to :back
    end
  end

  private
    def resource
      @web_setting = WebSetting.find(params[:id])
    end

    def collection
      @web_setting = WebSetting.latest
    end

    def params_permit
      params.require(:web_setting).permit(
                                          :title, 
                                          :description, 
                                          :keywords, 
                                          :header_tags, 
                                          :footer_tags, 
                                          :contact, 
                                          :email, 
                                          :favicon, 
                                          :logo, 
                                          :facebook, 
                                          :twitter, 
                                          :author, 
                                          :longitude, 
                                          :latitude, 
                                          :address
                                        )
    end

    def class_name
      @resource_name = 'WebSetting'
      @collection_name = 'WebSettings'
    end
end
