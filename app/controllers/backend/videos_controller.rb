class Backend::VideosController < Backend::ApplicationController
  before_action :class_name
  add_breadcrumb "Videos", :backend_videos_path

  def index
    @videos = collection.paginate(page: page, per_page: per_page)
    @videos_count = collection.size
  end

  def new
    prepare_add_breadcrumb_action
    @video = Video.new
  end

  def create
    @video = Video.new(permitted_params)
    @video.user_id = current_user.id
    if @video.save
      redirect_to :back, notice: 'Your video have been created!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def edit
    prepare_add_breadcrumb_action
    resource
  end

  def update
    resource
    if @video.update(permitted_params)
      redirect_to :back, notice: 'Your video have been updated!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def destroy
    resource
    if @video.destroy
      redirect_to :back, notice: 'Your video have been deleted!'
    else
      redirect_to :back, alert: 'Error'
    end
  end

  def multiple_action
    if params[:ids].present?
      if params[:commit] == 'Do Verified'
        
        collect = params[:ids]

        Video.where(id: collect).each do |c|
          c.status = true
          c.save
        end

        redirect_to :back, alert: 'Successfully set verified videos!'
      elsif params[:commit] == 'Do Featured'
        
        collect = params[:ids]

        Video.where(id: collect).each do |c|
          c.featured = true
          c.save
        end

        redirect_to :back, alert: 'Successfully set featured videos!'
      elsif params[:commit] == 'Do Destroy'
        collect = params[:ids]

        Video.where(id: collect).each do |c|
          c.destroy
        end

        redirect_to :back, alert: 'Successfully deleted videos!'
      else
        redirect_to :back, alert: 'Nothing checked!'
      end 
    end
  end

  private
    def resource
      @video = Video.find params[:id]
    end

    def collection
      @collection = Video.latest.search_by(params)
    end

    def permitted_params
      params.require(:video).permit(
                                    :id,
                                    :title, 
                                    :director, 
                                    :actors, 
                                    :category,
                                    :description,
                                    :cover_image,
                                    :file,
                                    :session,
                                    :user_id,
                                    :code,
                                    :tag,
                                    :year,
                                    :status,
                                    :featured,
                                    :type_video,
                                    :kind,
                                    :category_video_id,
                                    :link_download_video,
                                    :click_link_download_video,
                                    :link_download_subtitle,
                                    :click_link_download_subtitle,
                                    episodes_attributes: [
                                      :id,
                                      :title,
                                      :episode,
                                      :link_video,
                                      :cover_image,
                                      :link_download_video,
                                      :click_link_download_video,
                                      :link_download_subtitle,
                                      :click_link_download_subtitle,
                                      :_destroy
                                    ])
    end

    def class_name
      @resource_name = 'Video'
      @collection_name = 'Videos'
    end
end