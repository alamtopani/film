class PublicController < ApplicationController
  def home
    @all_videos = Video.actived.size
    @headline = Video.movies.actived.by_year.latest.first
    @latest_videos = collection.movies.limit(30)

    @headline_series = Video.series.actived.by_year.latest.first
    @latest_video_series = collection.series.limit(30)

    @popular_videos = Video.actived.popular.limit(6)
    @featured_videos = Video.actived.featured.updatest.limit(6)
  end

  private
    def collection
      # videos = Video.actived.by_year.top_position.latest
      videos = Video.actived.by_year.latest
    end
end