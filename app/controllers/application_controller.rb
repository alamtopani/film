class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include TheUser::DeviseFilter

  protected
    def per_page
      params[:per_page] ||= 40
    end

    def page
      params[:page] ||= 1
    end

    def prepare_activity(title, activitiable_type, activitiable_id, link, current_user)
      @activity = Activity.new
      @activity.title = title
      @activity.activitiable_type = activitiable_type
      @activity.activitiable_id = activitiable_id
      @activity.user_id = current_user.id
      @activity.link = link
      @activity.save
    end
end
