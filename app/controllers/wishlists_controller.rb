class WishlistsController < ApplicationController

  def create
    @wishlist = Wishlist.new(permitted_params)
    @wishlist.user_id = current_user.id
    if @wishlist.save
      prepare_activity("Has been saved wishlists video <a href='#{video_url(@wishlist.video)}'>#{@wishlist.video.title}</a>", 'Wishlist', @wishlist.id, video_url(@wishlist.video), current_user)
      redirect_to :back, notice: "This video has been entered into the list of your favorite list!"
    else
      redirect_to :back, alert: 'Error'
    end
  end

  private
  
    def permitted_params
      params.require(:wishlist).permit(
                                    :id,
                                    :user_id, 
                                    :video_id
                                    )
    end
end