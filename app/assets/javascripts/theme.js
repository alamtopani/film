$(document).ready(function(){
  $('.button-nav-side').click(function(){
    $('.rgba-black').animate({width:'toggle'},1000);
  });

  $('.rgba-black').click(function(){
    $(this).hide();
  });

  $(".avatar-form").on('click', '.btn-choice',function() {
    $(this).closest('.avatar-form').find('input[type="file"]').click();
  });

  $(".avatar-form").find("input[type='file']").on("change", function () {
    var reader = new FileReader();
    reader.onload = function (e) {
      $(".avatar-form").find("img")[0].src = e.target.result;
    };
    reader.readAsDataURL($(this)[0].files[0]);
  });

  $('.alert .close').click(function(){
    $('.section-alert').hide();
  });
  $('.section-alert').click(function(){
    $('.section-alert').hide();
  });
});