var BannerCover = React.createClass({
  render: function(){
    return(
      <div className="headline home">
        <img src={this.props.url_image} />
        <div className="title-content">
          <a href={this.props.link}>
            <h1>{this.props.title}</h1>
          </a>
        </div>
        <div className="feat-title-wrap">
          <h3>{this.props.h3}</h3>
        </div>
      </div>
    )
  }
});