var FeaturedVideos = React.createClass({

  handleHeader(){
    return <h4 className="titleize">FEATURED</h4>;
  },

  render: function(){
    var featured_videos = this.props.featured_videos
    var results = featured_videos.map((resource, index) =>{
      return(
        <li>
          <a href={resource.link}>
            <div className="cover">
              <img src={resource.cover} />
            </div>
            <div className="category">
              {resource.type_video}
            </div>
            <div className="title">
              {resource.title}
            </div>
            <small className="view-eye">
              <i className="fa fa-eye"></i> {resource.count} Views
            </small>
          </a>
        </li>
      )
    });

    return(
      <div>
        {this.handleHeader()}
        <ul className="latest-list-full">
          {results}
        </ul>
      </div>
    )
  }

});