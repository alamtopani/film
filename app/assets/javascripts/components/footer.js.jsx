var Footer = React.createClass({
  render: function(){
    var caption = this.props.title;

    return(
      <footer>
        <div className="container">
          <center>{caption}</center>
        </div>
      </footer>
    )
  }
});