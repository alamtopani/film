var Sidebar = React.createClass({
  loadYears: function(startYear){
    var currentYear = new Date().getFullYear(), years = [];
    startYear = startYear || 2000;

    while ( startYear <= currentYear ) {
      years.push(startYear++);
    } 

    return years;
  },

  cekYear(year, where_year){
    if(year == where_year){
      return 'true'
    }
  },

  render: function(){
    var categories = this.props.categories;
    var years = this.loadYears(2008).reverse();
    var root_url = 'http://movies-series.com' 
    var where_year = parseInt(this.props.where_year);
    return(
      <div className="section-sidebar-menu">
        <div className="col-md-12">
          <ul>
            <li>
              <a href = "/">
                <i className="fa fa-globe"></i> Beranda
              </a>
            </li>
            <li>
              <a href = "/users/login">
                <i className="fa fa-user"></i> SignIn/SignUp
              </a>
            </li>
            {categories.map(function(resource, index){
              return(
                <li>
                  <a href = {resource.link} className={resource.where_cat}>
                    <i className="fa fa-film"></i> {resource.name}
                    <small className="pull-right">{resource.count}</small>
                  </a>
                </li>
              )
            })}
          </ul>
          <div className="ads-google">
            <br />
            <strong>RELEASE YEAR</strong>
            <br />
            <br />
            <div className="row">
              {years.map((resource) =>{
                return(
                  <div className="col-xs-4">
                    <a className={"btn btn-info btn-xs btn-year " + this.cekYear(resource, where_year)} href={"/videos/results?year=" + resource}>{resource}</a>
                  </div>
                )
              })}
            </div>
            <br />
            <small className="color-grey">Share sovies-series.com to everyone ?</small>
            <br/>
            <br/>
            <div className='fb-share-button' data-href={root_url} data-layout = "button_count"></div>
            &nbsp;
            <a href = {"https://twitter.com/intent/tweet?url=" + root_url + "&text=Watch%20movies%20series%20videos.&via=movies-series"} target = "_blank" className = 'btn btn-info btn-xs'> <i className="fa fa-twitter" aria-hidden="true"></i> Share</a>
            &nbsp;
            <a href = {"https://plus.google.com/share?url=" + root_url} target = "_blank" className = 'btn btn-danger btn-xs'> <i className="fa fa-google-plus" aria-hidden="true"></i> Share</a>
          </div>
        </div>
      </div>
    )
  }
});