var Latestvideos = React.createClass({

  handleCaption(resource){
    if (resource.kind == 'Series'){
      return(
        <div>
          <div className="title">{resource.updated_serie_title}</div>
          <div className="category">{resource.updated_serie_at}</div>
        </div>
      )
    }else{
      return(
        <div>
          <div className="title">{resource.title}</div>
          <div className="category">{resource.type_video} (Rilis {resource.year})</div>
        </div>
      )
    }
  },

  handleMoreLink(){
    return(
      <div>
        <a className="btn btn-default btn-full" href={this.props.more_link}>
          View More {this.props.count} Movies
        </a>
      </div>
    )
  },

  render: function(){
    var latest_videos = this.props.latest_videos;

    var results = latest_videos.map((resource, index) => {
      return(
        <li key={resource.id}>
          <a href={resource.link}>
            <div className="cover">
              <img src={resource.cover} />
              <div dangerouslySetInnerHTML={{__html: resource.label}} />
              <small className="view-eye"><i className="fa fa-eye"></i> {resource.view_count} </small>
            </div>
          </a>
          {this.handleCaption(resource)}
        </li>
      )
    });

    return(
      <div>
        <ul className='content-list'>
          {results}
        </ul>
        {this.handleMoreLink()}
      </div>
    )
  }
});