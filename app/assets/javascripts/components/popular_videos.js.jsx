var PopularVideos = React.createClass({

  handleHeader(){
    return <h4 className="titleize">POPULAR</h4>;
  },

  render: function(){
    var popular_videos = this.props.popular_videos
    var results = popular_videos.map((resource, index) =>{
      return(
        <li>
          <a href={resource.link}>
            <div className="cover">
              <img src={resource.cover} />
            </div>
            <div className="category">
              {resource.type_video}
            </div>
            <div className="title">
              {resource.title}
            </div>
            <small className="view-eye">
              <i className="fa fa-eye"></i> {resource.count} Views
            </small>
          </a>
        </li>
      )
    });

    return(
      <div>
        {this.handleHeader()}
        <ul className="latest-list">
          {results}
        </ul>
      </div>
    )
  }

});