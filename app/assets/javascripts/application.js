//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require angular
//= require angular-animate
//= require angular-resource
//= require jquery.raty
//= require ratyrate
//= require theme
//= require cocoon
//= require ahoy
//= require jwplayer
//= require react
//= require react_ujs
//= require components

$(document).ready(function(){
  // $(".ads-movie-click").on("click", function(){
  //   setTimeout(function() {
  //     $('.ads-movie').hide();
  //   }, 5000);
  // });

  $('.ads-movie p').click(function(){
    $('.ads-movie').hide();
  });

  $('.reload').on('change', function(){
    $('.search').submit();
  });
  
  $(".video_kind_movie").each(function(){
    $(this).click(function(){
      if($(this).val() == 'Movies'){
        $('.section-video').hide();
      }else{
        $('.section-video').show();
      }
    });

    if($(this).val() != '' && $(this).val() != 'undefined'){
      type = $(this).filter(':checked').val();
      
      if(type == 'Series'){
        $('.section-video').show();
      }else{
        $('.section-video').hide();
      }
    }
  });
});
