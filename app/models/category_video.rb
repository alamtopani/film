class CategoryVideo < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  has_many :videos

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :alfa, ->{order(name: :asc)}

  mount_uploader :cover_image, ImageUploader

  is_impressionable
  has_many :impressions, as: :impressionable
end
