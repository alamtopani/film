class Episode < ActiveRecord::Base
  belongs_to :episodeable, polymorphic: true
  scope :latest, ->{order('episodes.episode DESC')}
  default_scope { order('episodes.episode DESC') }

  is_impressionable
  has_many :impressions, as: :impressionable

  mount_uploader :cover_image, ImageUploader

  def data_new_today?
    today = Time.zone.now
    find_resource = Episode.where(id: self.id).where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_week, today.at_end_of_week)
    
    if find_resource.present?
      return "<div class='label'>New</div>".html_safe
    else
      return ''
    end
  end
end
