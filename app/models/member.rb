class Member < User
  default_scope {where(type: 'Member')}

  is_impressionable
  has_many :impressions, as: :impressionable

  def status?
    if self.verified == true
      return '<label class="label label-success">verified</label>'.html_safe
    else
      return '<label class="label label-danger">not verified</label>'.html_safe
    end
  end

  def verified?
    self.verified == true
  end
end