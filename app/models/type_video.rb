class TypeVideo < EnumerateIt::Base
  associate_values :animation, :action, :drama, :comedy, :horor, :family, :romance, :thriller, :fantasy, :biography, :superhero
end
