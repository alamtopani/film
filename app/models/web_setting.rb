class WebSetting < ActiveRecord::Base
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  mount_uploader :favicon, ImageUploader
  mount_uploader :logo, ImageUploader
end
