class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: [:slugged, :finders]

  devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
          :validatable, :lockable, :timeoutable, :confirmable,
          :omniauthable, omniauth_providers: [:facebook, :google_oauth2, :twitter], :authentication_keys => [:login]

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates :email, presence: true, uniqueness: { case_sensitive: false }

  after_initialize :after_initialized
  has_one :profile
  accepts_nested_attributes_for :profile

  has_many :videos
  has_many :wishlists
  has_many :activities

  mount_uploader :avatar, ImageUploader

  include TheUser::UserAuthenticate
  include TheUser::UserSearching
  
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}

  ratyrate_rater

  def is_member?
    self.type == "Member"
  end

  def is_admin?
    self.type == "Admin"
  end

  private
    def after_initialized
      self.profile = Profile.new if self.profile.blank?
    end
end
