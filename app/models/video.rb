class Video < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  before_create :prepare_code
  
  belongs_to :user
  has_many :wishlists, foreign_key: 'video_id', dependent: :destroy
  belongs_to :category_video

  scope :movies, ->{where(kind: 'Movies')}
  scope :series, ->{where(kind: 'Series')}
  scope :featured, ->{where(featured: true)}
  scope :actived, ->{where(status: true)}
  scope :popular, -> {order(view_count: :desc)}
  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :updatest, ->{order(updated_at: :desc)}
  scope :top_position, ->{order(featured: :desc)}
  scope :by_year, ->{order(year: :desc)}
  scope :bonds, ->{eager_load(:category_video)}

  mount_uploader :cover_image, ImageUploader

  is_impressionable
  has_many :impressions, as: :impressionable

  has_many :episodes, as: :episodeable, dependent: :destroy
  accepts_nested_attributes_for :episodes, reject_if: :all_blank, allow_destroy: true

  include TheVideo::VideoSearching
  ratyrate_rateable "service"

  def status?
    if self.status == true
      return "<label class='label label-success'>Active</label>".html_safe
    else
      return "<label class='label label-danger'>Pending</label>".html_safe
    end
  end

  def featured?
    if self.featured == true
      return "<b><i class='fa fa-flag'></i> Featured</b>".html_safe
    else
      return ""
    end
  end

  def check_your_wish(current_user)
    current_user.wishlists.where(user_id: current_user.id, video_id: self.id)
  end

  def movies?
    self.kind == 'Movies'
  end

  def series?
    self.kind == 'Series'
  end

  def data_new_today?
    today = Time.zone.now
    find_resource = Video.where(id: self.id).where("DATE(created_at) >= DATE(?) AND DATE(created_at) <= DATE(?)", today.at_beginning_of_week, today.at_end_of_week)
    
    if find_resource.present?
      return "<div class='label'>New</div>".html_safe
    else
      return ''
    end
  end

  private
    def prepare_code
      self.code = 'V'+ 6.times.map{Random.rand(10)}.join
    end
end
