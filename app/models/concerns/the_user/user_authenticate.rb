module TheUser
  module UserAuthenticate
    extend ActiveSupport::Concern
    included do
      attr_accessor :login

      def authenticate(login, password)
        user = where("username = :login OR email = :login", { login: login }).first
        return nil unless user
        return nil unless user.valid_password?(password)
        user
      end

      def self.find_for_database_authentication(warden_conditions)
        conditions = warden_conditions.dup
        if login = conditions.delete(:login)
          where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
        else
          where(conditions).first
        end
      end

      def login=(login)
        @login = login
      end

      def login
        @login || self.username || self.email
      end

      def self.find_for_devise_oauth(auth_hash, signed_in_resource)
        check_email = auth_hash.info.email ? auth_hash.info.email : auth_hash.extra.raw_info.name.downcase
      
        if auth_hash.provider == 'facebook'
          @member = User.where("users.uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if @member.blank?
            member = self.find_or_create_by(provider: auth_hash.provider, uid: auth_hash.uid)
            member.username = auth_hash.extra.raw_info.name.downcase
            member.provider = auth_hash.provider
            member.email = check_email
            member.password = Devise.friendly_token[0,10]
            member.confirmation_token = nil
            member.confirmed_at = Time.now
            member.type = 'Member'
            member.uid = auth_hash.uid
            member.facebook_oauth_token = auth_hash.credentials.token
            member.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
            
            check_user = User.where("users.username =?", check_email).last

            if check_user.present?
              return 'Error'
            else
              member.save(validate: false)
              return member
            end

          else
            @member.uid = auth_hash.uid
            @member.facebook_oauth_token = auth_hash.credentials.token
            @member.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
            @member.save
            return @member
          end

        elsif auth_hash.provider == 'google_oauth2'
          
          @member = User.where("users.google_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if @member.blank?
            member = self.find_or_create_by(provider: auth_hash.provider, google_uid: auth_hash.uid)
            member.username = auth_hash.extra.raw_info.name.downcase
            member.provider = auth_hash.provider
            member.email = check_email
            member.password = Devise.friendly_token[0,10]
            member.confirmation_token = nil
            member.confirmed_at = Time.now
            member.type = 'Member'
            member.google_uid = auth_hash.uid
            member.google_oauth_token = auth_hash.credentials.token
            member.google_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone

            check_user = User.where("users.username =?", check_email).last

            if check_user.present?
              return 'Error'
            else
              member.save(validate: false)
              return member
            end
          else
            @member.google_uid = auth_hash.uid
            @member.facebook_oauth_token = auth_hash.credentials.token
            @member.facebook_oauth_token_expires = Time.at(auth_hash.credentials.expires_at).in_time_zone
            @member.save
            return @member
          end

        elsif auth_hash.provider == 'twitter'
          @member = User.where("users.twitter_uid =? OR users.email =?", auth_hash.uid, check_email).last 

          if @member.blank?
            member = self.find_or_create_by(provider: auth_hash.provider, twitter_uid: auth_hash.uid)
            member.username = auth_hash.extra.raw_info.name.downcase
            member.provider = auth_hash.provider
            member.email = check_email
            member.password = Devise.friendly_token[0,10]
            member.confirmation_token = nil
            member.confirmed_at = Time.now
            member.type = 'Member'
            member.twitter_uid = auth_hash.uid
            member.twitter_token = auth_hash.credentials.token
            member.twitter_secret = auth_hash.credentials.secret

            check_user = User.where("users.username =?", check_email).last

            if check_user.present?
              return 'Error'
            else
              member.save(validate: false)
              return member
            end
          else
            @member.twitter_uid = auth_hash.uid
            @member.twitter_token = auth_hash.credentials.token
            @member.twitter_secret = auth_hash.credentials.secret
            @member.save
            return @member
          end

        end
      end
      
    end
  end
end