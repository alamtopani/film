module TheVideo
  module VideoSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        query_opst = [
          "LOWER(videos.title) LIKE LOWER(:key)",
          "LOWER(videos.tag) LIKE LOWER(:key)",
          "LOWER(videos.actors) LIKE LOWER(:key)",
          "LOWER(videos.director) LIKE LOWER(:key)",
          "LOWER(category_videos.name) LIKE LOWER(:key)"
        ].join(" OR ")
        where(query_opst, {key: "%#{_key}%"})
      end

      def by_category(category)
        where("category_videos.slug =?", category)
      end

      def by_kind(kind)
        where("videos.kind =?", kind)
      end

      def year(year)
        where("videos.year =?", year)
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        if options[:category].present?
          results = results.by_category(options[:category])
        end

        if options[:kind].present?
          results = results.by_kind(options[:kind])
        end

        if options[:year].present?
          results = results.year(options[:year])
        end

        return results
      end
    end
  end
end