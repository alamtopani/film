module TheVideo
  module WishlistSearching
    extend ActiveSupport::Concern

    module ClassMethods
      def by_keywords(_key)
        query_opst = [
          "LOWER(videos.title) LIKE LOWER(:key)",
          "LOWER(videos.tag) LIKE LOWER(:key)",
          "LOWER(videos.actors) LIKE LOWER(:key)",
          "LOWER(videos.director) LIKE LOWER(:key)"
        ].join(" OR ")
        where(query_opst, {key: "%#{_key}%"})
      end

      def search_by(options={})
        results = bonds

        if options[:key].present?
          results = results.by_keywords(options[:key])
        end

        return results
      end
    end
  end
end