class Activity < ActiveRecord::Base
  belongs_to :activitiable, polymorphic: true
  belongs_to :user

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
end
