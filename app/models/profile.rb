class Profile < ActiveRecord::Base
  belongs_to :user

  def place_info
    [address, province, city, country].select(&:'present?').join(', ')
  end
end
