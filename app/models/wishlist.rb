class Wishlist < ActiveRecord::Base
  belongs_to :user
  belongs_to :video

  scope :latest, ->{order(created_at: :desc)}
  scope :oldest, ->{order(created_at: :asc)}
  scope :bonds, ->{eager_load(:video)}

  include TheVideo::WishlistSearching
end
